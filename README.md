# Country Website
![Design homepage of the Country Website](./src/assets/homepage.png)

## Description
This is a country website project developed using react.

## Contains
A continent filter

Navigation

Filter the number of countries

## Technologies Used

React

React-Router-Dom

Hooks

Sass

Axios

## Installation

git clone https://gitlab.com/Yutsu/reactcountries.git

npm install

npm start
