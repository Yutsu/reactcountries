import React from 'react';
import Home from './pages/Home';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import NotFound from './pages/NotFound';
import About from './pages/About';
import Logo from './components/Logo';

const App = () => {
    return (
        <BrowserRouter>
            <Logo />
            <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/about" exact component={About} />
                <Route component={NotFound} />
            </Switch>
        </BrowserRouter>
    );
};

export default App;