import React from 'react';
import Navigation from '../components/Navigation';

const About = () => {
    return (
        <div>
            <Navigation />
            <h1>About</h1>
            <p>Lorem ipsum dolor sit amet.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa autem minus at ipsam ad consectetur, architecto dignissimos blanditiis ab ducimus!</p>
        </div>
    );
};

export default About;