import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Card from './Card';

const Countries = () => {

    //state : élément et affiche
    //setState : modifie/actualise l'élément
    //hooks
    const [state, setState] = useState([]);
    const [sortedData, setSortedData] = useState([]);
    const [playOnce, setPlayOnce] = useState(true);
    const [rangeValue, setRangeValue] = useState(30);
    const [selectedRadio, setSelectedRadio] = useState('');
    const radios = ['Africa', 'America', 'Asia', 'Europe', 'Oceania'];

    useEffect(() => {
        if(playOnce){
            //already json
            axios
                .get('https://restcountries.eu/rest/v2/all?fields=name;population;region;capital;flag')
                .then((result) => {
                    setState(result.data);
                    setPlayOnce(false);
                });
        };

        const sortedCountry = () => {
            const countryObject = Object.keys(state).map(i => state[i]);
            const sortedArray = countryObject.sort((a,b) => {
                return b.population - a.population;
            });
            sortedArray.length = rangeValue;
            setSortedData(sortedArray);
        };
        sortedCountry();
        //[] = 1seule fois | [state] = infini if no boolean
    }, [state, rangeValue, playOnce]);

    return (
        <div className="countries">
            <div className="sort-container">
                <input type="range" min="1" max="250" value={rangeValue}
                    onChange={(e) => setRangeValue(e.target.value)}
                />
                <ul>
                    {radios.map((radio) => {
                        return (
                            <li key={radio}>
                                <input type="radio" value={radio} id={radio}
                                    checked={radio === selectedRadio}
                                    onChange={(e) => setSelectedRadio(e.target.value)}/>
                                <label htmlFor={radio}>{radio}</label>
                            </li>
                        )
                    })}
                </ul>
                <p>{rangeValue}</p>
            </div>
            <div className="cancel">
                {/* si coché alors && => donc inclu */}
                {selectedRadio && <h5 onClick={() => setSelectedRadio("")}>Annuler la recherche</h5>}
            </div>
            <ul className="countries-list">
                {sortedData
                    .filter((country) => ( country.region.includes(selectedRadio)) )
                    .map((country) => (
                    //props | key=value
                    <Card dataCountry={country} key={country.name}/>
                ))}
            </ul>
        </div>
    );
};

export default Countries;