import React from 'react';

//ou ({dataCountry}) key=value
const Card = (props) => {
    const { name, flag, capital, population } = props.dataCountry;

    const numberFormat = (nb) => {
        return nb.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    return (
        <li className="card">
            <img src={flag} alt="flag"/>
            <div className="data-container">
                <ul>
                    <li>{name}</li>
                    <li>{capital}</li>
                    <li>Pop: {numberFormat(population)}</li>
                </ul>
            </div>
        </li>
    );
};

export default Card;